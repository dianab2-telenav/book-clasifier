package spark_solution;

import java.util.regex.Pattern;


public class Resources {

    public static final Pattern EXTRACT_LANGUAGE_PATTERN = Pattern.compile("(.+_)([a-z]+)(\\.txt)");
    public static final String SPLIT_REGEX = "[.!?, \t\n]+";
    public static final String WORD_REGEX = "[A-Za-z'-]{3,}";
}
