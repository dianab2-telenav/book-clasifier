package spark_solution;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;


public class BookLanguageDetector implements Serializable {

    private Map<String, HashSet<String>> languageAndMapOfWordsWithOccurences;

    public BookLanguageDetector(Map<String, HashSet<String>> languageAndMapOfWordsWithOccurences) {
        this.languageAndMapOfWordsWithOccurences = languageAndMapOfWordsWithOccurences;
    }

    public void test(final String sourcePath, final SparkSession sparkSession, Predicate<File> predicate) {
        FileProcessor fileProcessor = new FileProcessor();
        for (File book : FileFilter.filterFiles(sourcePath, predicate)) {
            final JavaPairRDD<String, Integer> wordsFrequencyRDD = fileProcessor.computeFrequencies(book, sparkSession);
            final Long numberOfDistinctWords = wordsFrequencyRDD.count();
            final Integer totalNumberOfWords = wordsFrequencyRDD.map(Tuple2::_2).reduce(Integer::sum);
            final Double average = (double) 1 / numberOfDistinctWords;
            final JavaRDD<String> wordsOfTheBookMap =
                    filterWords(wordsFrequencyRDD, tuple -> (double) tuple._2 / totalNumberOfWords >= average);
            findLanguageOfTheBook(book.getName(), wordsOfTheBookMap);
        }
    }

    private JavaRDD<String> filterWords(JavaPairRDD<String, Integer> wordFrequenciesPairs,
            Function<Tuple2<String, Integer>, Boolean> filter) {
        return wordFrequenciesPairs.filter(filter).map(Tuple2::_1);
    }

    private void findLanguageOfTheBook(String bookName, final JavaRDD<String> wordsOfTheBookMap) {
        final BookMetadata bookMeta = new BookMetadata(bookName, 0.0, "UNKNOWN");

        Optional<Tuple2<String, Double>> languageAndBelieve = languageAndMapOfWordsWithOccurences.entrySet()
                .stream()
                .map(language -> new Tuple2<>(language.getKey(),
                        getFrequentWords(language.getKey(), bookName, wordsOfTheBookMap)))
                .max(Comparator.comparingDouble(Tuple2::_2));
        System.out.println(bookName);
        bookMeta.setLanguage(languageAndBelieve.get()._1());
        bookMeta.setBelieve(languageAndBelieve.get()._2());
        bookMeta.show();
    }

    private Double getFrequentWords(String language, String name, final JavaRDD<String> wordsOfTheBookMap) {
        final BookMetadata auxMeta = new BookMetadata(name, 0.0, language);
        int wordsMatchedCount = wordsOfTheBookMap
                .map(word -> (languageAndMapOfWordsWithOccurences.get(language).contains(word)) ? 1 : 0)
                .reduce((x, y) -> x + y);
        auxMeta.setFound(wordsMatchedCount);
        return auxMeta.getBelieve((int) wordsOfTheBookMap.count());
    }


}
