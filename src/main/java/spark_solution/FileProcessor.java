package spark_solution;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.io.File;
import java.util.Arrays;


public class FileProcessor {

    public JavaPairRDD<String, Integer> computeFrequencies(File book, SparkSession sparkSession){
        final Dataset<String> textDataset = sparkSession.read()
                .text(book.getAbsolutePath())
                .as(Encoders.STRING())
                .cache();
        final JavaRDD<String> wordsOfTheBookRDD = textDataset.javaRDD()
                .flatMap(line -> Arrays.asList(line.split(Resources.SPLIT_REGEX)).iterator());
        return wordsOfTheBookRDD.mapToPair(word -> new Tuple2<>(word.toLowerCase(), 1))
                .filter(tuple -> (tuple._1.matches(Resources.WORD_REGEX)))
                .reduceByKey((a, b) -> a + b);
    }

}
