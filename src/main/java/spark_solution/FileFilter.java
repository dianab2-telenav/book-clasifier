package spark_solution;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class FileFilter {

    public static List<File> filterFiles(final String sourcePath, Predicate<File> filter) {
        File sourceFile = new File(sourcePath);
        return Arrays.asList(sourceFile.listFiles()).stream().filter(filter).collect(Collectors.toList());
    }

}
