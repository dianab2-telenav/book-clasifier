package spark_solution;

public class Main {

    public static void main(String args[]) {
        AppStarter appStarter = new AppStarter();
        appStarter.startApplication(file -> file.isHidden());
    }
}
