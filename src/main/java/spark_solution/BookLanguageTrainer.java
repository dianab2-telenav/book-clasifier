package spark_solution;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Matcher;


public class BookLanguageTrainer implements Serializable {

    public Map<String, HashSet<String>> train(final String sourcePath, final SparkSession sparkSession,
            Predicate<File> predicate) {
        Map<String, HashSet<String>> frequentWords = new HashMap<>();
        final FileProcessor fileProcessor = new FileProcessor();
        for (File book : FileFilter.filterFiles(sourcePath, predicate)) {
            final String language = findLanguage(book.getName());
            final JavaPairRDD<String, Integer> wordsFrequencyRDD = fileProcessor.computeFrequencies(book, sparkSession);
            final Long numberOfDistinctWords = wordsFrequencyRDD.count();
            final Integer totalNumberOfWords = wordsFrequencyRDD.map(tuple -> tuple._2)
                    .reduce((x, y) -> x + y);
            final Double average = (double) 1 / numberOfDistinctWords;

            HashSet<String> wordsOfTheBookMap =
                    filterWords(wordsFrequencyRDD, tuple -> (double) tuple._2 / totalNumberOfWords >= average);

            frequentWords = updateFrequentWords(language, wordsOfTheBookMap, frequentWords);
        }
        return frequentWords;
    }

    private String findLanguage(final String name) {
        Matcher matcher = Resources.EXTRACT_LANGUAGE_PATTERN.matcher(name);
        return (matcher.find()) ? matcher.group(2) : "UNKNOWN";
    }

    private HashSet<String> filterWords(JavaPairRDD<String, Integer> wordFrequenciesPairs,
            Function<Tuple2<String, Integer>, Boolean> filter) {
        return new HashSet<>(wordFrequenciesPairs.filter(filter).map(Tuple2::_1).collect());
    }

    private Map<String, HashSet<String>> updateFrequentWords(String language, HashSet<String> wordsOfTheBookMap,
            Map<String, HashSet<String>> frequentWords) {
        if (frequentWords.containsKey(language)) {
            wordsOfTheBookMap.stream()
                    .forEach(word -> frequentWords.get(language).add(word));
        } else {
            frequentWords.put(language, wordsOfTheBookMap);
        }
        return frequentWords;
    }
}
