package spark_solution;

import org.apache.spark.sql.SparkSession;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Predicate;


public class AppStarter {

    private static final String PATH = "./src/main/resources/train";
    private static final String TEST = "./src/main/resources/test";

    public void startApplication(Predicate<File> predicate) {
        final SparkSession sparkSession = startSparkSession();
        final BookLanguageTrainer trainer = new BookLanguageTrainer();
        final Map<String, HashSet<String>> frequentWords = trainer.train(PATH, sparkSession, predicate);
        final BookLanguageDetector detector = new BookLanguageDetector(frequentWords);
        detector.test(TEST, sparkSession, predicate);
        sparkSession.stop();
    }

    private SparkSession startSparkSession() {
        SparkSession sparkSession =
                SparkSession.builder().master("local[*]").appName("Classifier Application").getOrCreate();
        sparkSession.sparkContext().setLogLevel("ERROR");
        return sparkSession;
    }

}
