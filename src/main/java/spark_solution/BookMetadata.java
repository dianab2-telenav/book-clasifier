package spark_solution;

import java.io.Serializable;


public class BookMetadata implements Serializable {

    private String title;
    private Double believe;
    private String language;
    private Integer found = 0;

    public BookMetadata(final String title, final Double believe, final String language) {
        this.title = title;
        this.believe = believe;
        this.language = language;
    }

    public Double getBelieve(int total) {
        return (double) found / total;
    }

    public Double getBelieve() {
        return believe;
    }

    public void setBelieve(final Double believe) {
        this.believe = believe;
    }

    public Integer getFound() {
        return found;
    }

    public void setFound(int found) {
        this.found = found;
    }


    public void setLanguage(final String language) {
        this.language = language;
    }

    public void show() {
        System.out.println(toString());
    }

    @Override public String toString() {
        return this.title + " -> Language: [" + language + "] with probability " + believe;
    }
}

