package stream_solution;

import java.util.*;
import java.io.*;
import java.util.stream.Collectors;


public class BookClasifier {

    private HashMap<String, Map<String, Long>> bookLanguageAndSetOfWordsCountTrain;
    private final String SPLIT_REGEX = "[., \t\n]+";
    private final int TARGET = 2;

    public BookClasifier(HashMap<String, Map<String, Long>> blasowct) {
        bookLanguageAndSetOfWordsCountTrain = blasowct;
    }

    public void clasifyBooks(File book) {
        Book bookData = new Book(book.getName(), 0.0, "UNKNOWN");
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(book));
            Map<String, Long> temporaryMap =
                    bufferedReader.lines().flatMap(line -> Arrays.asList(line.split(SPLIT_REGEX)).stream())
                            .collect(Collectors.groupingBy(word -> word, Collectors.counting())).entrySet().stream()
                            .filter(e -> e.getValue() >= TARGET)
                            .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
            bookLanguageAndSetOfWordsCountTrain.entrySet().stream().forEach(e -> {
                Map<String, Long> temporaryMap2 = new HashMap<>(temporaryMap);
                Double believe = (double) temporaryMap2.entrySet().stream()
                        .filter(entry -> e.getValue().containsKey(entry.getKey())).count() / temporaryMap.entrySet()
                        .stream().count();
                if (believe > bookData.getBelieve()) {
                    bookData.setBelieve(believe);
                    bookData.setLanguage(e.getKey());
                }
            });
        } catch (Exception e) {

        }
        bookData.show();
    }


}
