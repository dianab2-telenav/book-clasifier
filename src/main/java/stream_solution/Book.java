package stream_solution;

public class Book {
    private String title;
    private Double believe;
    private String language;

    public Book(final String title,final Double believe, final String language) {
        this.title = title;
        this.believe = believe;
        this.language = language;
    }

    public Double getBelieve() {
        return believe;
    }

    public void setBelieve(final Double believe) {
        this.believe = believe;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    @Override public String toString() {
        return this.title + " -> Language: [" + language + "] with probability " + believe;
    }

    public void show(){
        System.out.println(toString());
    }
}
