package stream_solution;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;


public class BookTrainer {
    private HashMap<String, Map<String,Long>> bookLanguageAndSetOfWordsCountTrain = new HashMap<>();
    private final File trainingFolder = new File("./src/main/resources/train");
    private final File testFolder = new File("./src/main/resources/test");

    private final String REGEX = "(.+_|.txt)";
    private final String SPLIT_REGEX = "[., \t\n]+";
    private final int TARGET = 2;

    public void listFilesForFolder() {
        for ( File book : trainingFolder.listFiles()) {
                parseBook(book);
        }
        BookClasifier bookClasifier = new BookClasifier(this.bookLanguageAndSetOfWordsCountTrain);
        for ( File book : testFolder.listFiles()) {
            System.out.println(book.getName());
            bookClasifier.clasifyBooks(book);
        }
    }

    public void parseBook(File book){
        String language = book.getName().replaceAll(REGEX, "");
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(book));
            Map<String, Long> temporaryMap = bufferedReader.lines()
                    .flatMap(line -> Arrays.asList(line.split(SPLIT_REGEX)).stream())
                    .collect(Collectors.groupingBy(word -> word, Collectors.counting()))
                    .entrySet()
                    .stream()
                    .filter(e -> e.getValue() >= TARGET )
                    .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
            if(!bookLanguageAndSetOfWordsCountTrain.containsKey(language)) {
                bookLanguageAndSetOfWordsCountTrain.put(language, temporaryMap);
            } else {
                bookLanguageAndSetOfWordsCountTrain.get(language).entrySet()
                        .stream()
                        .forEach(e -> {
                            if(temporaryMap.containsKey(e.getKey())) {
                                e.setValue(Math.max(e.getValue(), temporaryMap.get(e.getKey())));
                            }
                            else {
                                bookLanguageAndSetOfWordsCountTrain.get(language).put(e.getKey(), e.getValue());
                            }
                        });
            }
        }catch (Exception e){

        }
    }

    public static void main(String args[]) {
        BookTrainer bt = new BookTrainer();
        bt.listFilesForFolder();
    }


}
