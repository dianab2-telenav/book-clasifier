package tutorial;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import scala.Tuple2;

public class SimpleApp {
    public static void main(String[] args) {
        String logFile = "src/main/resources/userdata1.parquet"; // Should be some file on your system
        SparkSession spark = SparkSession.builder().master("local[*]").appName("Simple Application").getOrCreate();
        spark.sparkContext().setLogLevel("ERROR");
        Dataset<Row> logData = spark.read().parquet(logFile).cache();
        Dataset<Person> logPerson = spark.read().parquet(logFile).as(Encoders.bean(Person.class)).cache();
        JavaRDD<Person> personJavaRDD = logPerson.javaRDD();


        //        logData.count();
        //        logPerson.show();
        //        logData.printSchema();

        JavaPairRDD<String, Integer> countryCount = personJavaRDD
                .mapToPair(p -> new Tuple2<String, Integer>(p.getCountry(), 1))
                .reduceByKey(Integer::sum)
                .sortByKey();

        countryCount.collect().forEach(System.out::println);

        Dataset<Row> rows = logPerson.groupBy(logPerson.col("country")).count().sort();
        rows.show(1000);
        spark.stop();
    }
}