package tutorial;

public class Person {
    private long registration_dttm;
    private Integer id;
    private String first_name, last_name, email, gender, ip_address, country, birthdate, title, comments;
    private Double salary;

    public long getRegistration_dttm() {
        return registration_dttm;
    }

    public void setRegistration_dttm(final long registration_dttm) {
        this.registration_dttm = registration_dttm;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(final String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(final String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(final String gender) {
        this.gender = gender;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(final String ip_address) {
        this.ip_address = ip_address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(final String birthdate) {
        this.birthdate = birthdate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(final String comments) {
        this.comments = comments;
    }

    public void setSalary(final Double salary) {
        this.salary = salary;
    }

    public Person(final long registration_dtm, final Integer id, final String first_name, final String last_name, final String email,
            final String gender, final String ip_address, final String country, final String birthdate, final Double salary,
            final String title, final String comments) {
        this.registration_dttm = registration_dttm;

        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.gender = gender;
        this.ip_address = ip_address;
        this.country = country;
        this.birthdate = birthdate;
        this.title = title;
        this.comments = comments;
        this.salary = salary;
    }
    public Person(){
    }

    public Double getSalary() {
        return salary;
    }

    public Integer getId() {
        return id;
    }
}
